# Table of Contents
- [Formal Exam Presentation Setup](#formal-exam-presentation-setup)
  - [Background Information](#background-information)
  - [Local Exam Setup](#local-exam-setup)
  - [Test Bank Automation](#test-bank-automation)
    - [Setup The ExamCreator Google Apps Script](#setup-the-examcreator-google-apps-script)
    - [Setup Google Cloud Platform (GCP) Project ID](#setup-google-cloud-platform-gcp-project-id)
    - [Enable Google Drive API](#enable-google-drive-api)
    - [Create an OAuth Client ID and Download credentials.json](#create-an-oauth-client-id-and-download-credentialsjson)
    - [Access Token Generation](#access-token-generation)
      - [Software Prerequisites](#software-prerequisites)
      - [Generate a Token.Pickle](#generate-a-tokenpickle)
      - [Generate Clasprc Tokens](#generate-clasprc-tokens)
    - [Explanation of Exam Generation Scripts](#explanation-of-exam-generation-scripts)
      - [Script to build_exam_content](#script-to-build_exam_content)
      - [Script to generate_gsuite_exam](#script-to-generate_gsuite_exam)
    - [Configure Exam Stats File](#configure-exam-stats-file) 
    - [CI / CD Pipeline Configuration](#ci-cd-pipeline-configuration)
- [Formal Exam Presentation Deployment](#formal-exam-presentation-deployment)
  - [GitLab Continuous Integration/Continuous Delivery (CI/CD) Pipeline](#gitlab-continuous-integrationcontinuous-delivery-cicd-pipeline)
    - [View Pipeline Artifacts](#view-pipeline-artifacts)
  - [Exam Creation](#exam-creation)
    - [Generate Exams](#generate-exams)
    - [Configure Exam Settings](#configure-exam-settings)
  - [Exam Distribution](#exam-distribution)

# Formal Exam Presentation Setup

This document outlines the Formal Exam Presentation setup process for external organizations utilizing the 90th Cyberspace Operations Squadron Evaluations Element exam. These instructions are for the Google Suite formal exam component which uses a Google Apps Script to generate a Google Form that will be a knowledge exam.

\[ [TOC](#table-of-contents) \]

## Background Information

| Term |Definition |
|------------|-------------|
| Exam | Set of questions designed to Examine an individual’s body of knowledge |
| Exam Material | Questions and answers used to create exams |
| Code Snippet | An image that contains code |
| Repo | Central location where data is stored on gitlab.com |
| Google Drive | A Google service to store and modify files |
| Google Apps Script | A Google service to store and modify scripts |
| Google Cloud Platform (GCP) | A Google service to manage Google Apps |
| Template | File used as a pattern for repeating processes |
| knowledge test bank | Your forked version of the knowledge test bank template |

\[ [TOC](#table-of-contents) \]

## Local Exam Setup

- Ensure there is Internet access and appropriate credentials to reach [Google Drive](https://accounts.google.com/).
  
  - Note:  A commercial Internet Service Provider (ISP) is necessary due to Non-classified Internet Protocol Router Network (NIPRNET) restrictions.

- If not already done, create a Google Drive account for your organization.  This account will be used to run the Knowledge Phase of your Exam.

- Log into your Google Drive account.

- Navigate to the Google Drive service by clicking the circle containing a dial pad (at top right, below bookmarks bar), then select “Drive” (see circled icon in the Figure below).

<br><br>
![Fig 1](images/fig_01_exam-pres_AccessGDrive.png)
<br><br>

The suggested folder structure is shown below. 

```
My Drive(root)
|
|— code_snippets

```

Notes to the above directory tree image:
- Images for all workroles are all kept within the `code_snippets/` folder.

Designate the destination directory ID where the automated exam generation will save any images to your questions.
- After creating the directory for your images, copy the directory ID.
  
  - Expand `My Drive` and Select the directory you created for the images.
  
  - The directory ID can be found in your browser's address bar; it will be the last folder, see the image below for an example.
  <br><br>
  ![Fig 2](images/fig_02_exam-pres_SourceImageDirId.png)
  <br><br>
- From your forked version of the `Knowledge Test Bank Template` (your GitLab account will need a role allowing CI pipeline variable changes):

  - Scroll the left vertical menu to the bottom, hover over `Settings` and click `CI / CD`.

  - In the `Variables` section, click the `Expand` button.

  - Click the `Add Variable` button and an `Add variable` window will appear.

    - In the `Key` text box, enter `DST_IMAGE_DIR_ID`.

    - In the `Value` text box, paste the directory ID of the folder that will contain your images.

    - In the `Flags` section, check `Mask variable`.

    - Click the `Add variable` button in the bottom right corner.

    - Your new variable will appear in the `Variables` section.

\[ [TOC](#table-of-contents) \]

## Test Bank Automation
These instructions help you setup your knowledge test bank repository so question content, such as images, will be available to your Google Drive and to enable exam form generation from the pipeline. The information in this section will help you setup your environment to be able to generate these forms.

When you are finished with these procedures, you should have the following CI / CD pipeline variables defined in your knowledge test bank repository.

- DST_IMAGE_DIR_ID

- SCRIPT_ID

- PROJECT_ID

- IMAGE_PUSH_TOKEN

- CLASP_TOKEN_GLOBAL

- CLASP_TOKEN_LOCAL

\[ [TOC](#table-of-contents) \]

### Setup The ExamCreator Google Apps Script
The Google Apps Script, named `ExamCreator`, is responsible for generating your knowledge exams. The pipeline will push the latest version of ExamCreator to the project you are about to establish; therefore, no code will need to be copied into the files created during these procedures.

- Enable the [Google Apps Script API](https://script.google.com/home/usersettings) then go back to your Google Drive.

- At the top left of your Google Drive, you will see a button labeled `+ New`; click this button and select `More` -> `Google Apps Script`.
  <br><br>
  ![Fig 3](images/fig_03_exam-pres_NewButton.png)
  <br><br>
  ![Fig 4](images/fig_04_exam-pres_CreateGoogleAppsScript.png)
  <br><br>
- A new window will open, and you will be in the Google Apps Script editor; the image below shows the default view of a new script.
  <br><br>
  ![Fig 5](images/fig_05_exam-pres_ScriptEditorWindowDefaultView.png)
  <br><br>
- Create a name for your project by changing `Untitled project` to a meaningful name of your choice; our suggestion is `Formal Exam Creator`.

- On the left, you will see a series of icons organized vertically; select the `Project Settings` icon as indicated in the image below.
  <br><br>
  ![Fig 6](images/fig_06_exam-pres_ScriptEditorWindowProjectSettings.png)
  <br><br>
- Within `Project Settings`, copy the script ID to your clip board.

- Create another CI / CD variable that stores your script ID:

  - Set the `Key` to `SCRIPT_ID`.

  - Set the `Value` to the script ID copied from your `Google Apps Script Project Settings`.

  - Check `Mask variable`.

\[ [TOC](#table-of-contents) \]

### Setup Google Cloud Platform (GCP) Project ID
- Navigate to the Google Apps Script editor for the script you just created and go to its `Project Settings`.

- Under the heading `Google Cloud Platform (GCP) Project`, click the `Change Project` button, see image below.
  <br><br>
  ![Fig 7](images/fig_07_exam-pres_ScriptEditorWindowProjectSettingsGCP.png)
  <br><br>
- Looking at item number 1, click the word `here` in the sentence `Choose or create a new Google Cloud Platform project here`, refer to the below image.
  <br><br>
  ![Fig 8](images/fig_08_exam-pres_GoogleCloudPlatformProject.png)
  <br><br>
- If this is the first time accessing Google Cloud Platform (GCP) then you will receive a welcome window.

  - Choose your country from the dropdown box.

  - Read and check the box to agree to the terms of service.

  - Click `AGREE AND CONTINUE`

- The dashboard for first time users will look like the image below. For everyone else, the `Select a Project` button will have a project selected by default and the dashboard will show this project's properties.
  <br><br>
  ![Fig 9](images/fig_09_exam-pres_GoogleCloudPlatformDashboard.png)
  <br><br>
- To create a new project

  - First time users click the `Select a Project` button.

  - Otherwise, click the button with the project name; this will be in the same location as the `Select a Project`.

- On the `Select A Project` window, click the `NEW PROJECT` button in the upper righthand corner.

- On the `New Project` page, change the project name to the same name you used when you created your script project, then click `CREATE`.

- Your dashboard will now appear like the image below.
  <br><br>
  ![Fig 10](images/fig_10_exam-pres_GoogleCloudPlatformDashboardv2.png)
  <br><br>
- Copy the project number and then go back to your Google Apps Script Editor `Project Settings` page.

- Under the `Google Cloud Platform (GCP) Project` section, paste the project number in the box for step 2 and click `Set project`.

- Now go back to the GCP Dashboard and copy your Project ID.

- Within your forked version of the `Knowledge Test Bank Template`, create another CI / CD variable to store your project ID.

  - Set the `Key` to `PROJECT_ID`.

  - Set the `Value` to the Project ID you copied from the GCP Dashboard.

  - Check `Mask variable`.

\[ [TOC](#table-of-contents) \]

### Enable Google Drive API
- Navigate to the Google Cloud Platform (GCP) project associated with your Google Apps Script project; the recommended project name was `Formal Exam Creator`.

- In the left vertical menu, highlight `APIs & Services` and click `Dashboard`.
  <br><br>
  ![Fig 11](images/fig_11_exam-pres_GoogleCloudPlatformAPIsDashboard.png)
  <br><br>
- Click the `+ ENABLE APIS AND SERVICES` button shown below.
  <br><br>
  ![Fig 12](images/fig_12_exam-pres_GCP-APIsServicesEnableGDriveAPI.png)
  <br><br>
- In the Search bar, type `Google Drive`.

- Click the result for `Google Drive API`, see the image below.
  <br><br>
  ![Fig 13](images/fig_13_exam-pres_GCP-APIsServicesSearchGDriveAPI.png)
  <br><br>
- Click the `ENABLE` button shown below.
  <br><br>
  ![Fig 14](images/fig_14_exam-pres_GCP-APIsServicesEnableGDriveAPIv2.png)
  <br><br>
- After the API is enabled, you will be taken to the Google Drive API overview page.

- You will know it was enabled because you will see an option at the top to disable the API.

- To go back to the APIs & Services page, click on `APIs & Services Google Drive API` in the top left of the page.
  <br><br>
  ![Fig 15](images/fig_15_exam-pres_GCP-APIsServicesGDriveAPIEnabled.png)
  <br><br>
\[ [TOC](#table-of-contents) \]

### Create an OAuth Client ID and Download credentials.json
When generated, the credentials.json file contains all the information necessary to generate access tokens. Programs, such as CLASP and the Google API, use credentials.json to generate the token(s) needed for them to access your drive. Since this system uses both CLASP and the Google API, you will need to generate tokens for each.

- If not on the `APIs & Services` page, navigate to the GCP project associated with your Formal Exam Creator script project.

- Setup OAuth consent by doing the following:

  - (If not on the `APIs & Services` page) In the left vertical menu, highlight `APIs & Services` and click `OAuth consent screen`, refer to the image below.
  <br><br>
  ![Fig 16](images/fig_16_exam_pres_GoogleCloudPlatformOAuthConsent.png)
  <br><br>
  - Otherwise, just click `OAuth consent screen` in the left vertical menu.

  - Select `External` and click `CREATE`.

  - The three fields you must fill-in are:

    - `App name` (we suggest using the same name as your script project)

    - `User support email`

    - `Developer contact email`

  - Click `SAVE AND CONTINUE`.

  - On the `Scopes` section, click `SAVE AND CONTINUE`.

  - On the `Test users` section:

    - Click `+ ADD USERS`.

    - Enter your account's email address.

      - This should be the same account storing the Formal Exam Creator script and exams.

      - No other users will need access since this will be generated from the pipeline.

    - Click `SAVE AND CONTINUE`

  - On the `Summary` section, click `BACK TO DASHBOARD`.

- Create an OAuth Client ID

  - From the left vertical menu, click `Credentials`.

  - Click `+ CREATE CREDENTIALS` and then select `OAuth Client ID`.
  <br><br>
  ![Fig 17](images/fig_17_exam-pres_GCP-APIsServicesCreateCredentials.png)
  <br><br>
  - In the `Web Application Type` dropdown box, select `Desktop Application`.

  - Click `CREATE`.

- When your credential is created, you will be taken back to the `APIs & Services` -> `Credentials` page where you can download your newly created credential.
  <br><br>
  ![Fig 18](images/fig_18_exam-pres_GCP-APIsServicesCredentialsOAuth2.0ClientIds.png)
  <br><br>
  - From this page you should see your newly created credential under the heading `OAuth 2.0 Client IDs`.

  - On the far right you will see a series of icons, one of which looks like an arrow pointing downward; click this icon to download your credential.

  - Change the name to `crendentials.json` and save it on your computer. Remember, this file should never be saved in GitLab.

\[ [TOC](#table-of-contents) \]

### Access Token Generation
These procedures will help you create the access tokens necessary for the pipeline to access your Google Drive. These access tokens are necessary to push the newest images and script project files, as well as generating the exams. To prevent others from accessing your Google Drive, the files generated during these procedures should never be stored in GitLab. Instead, we have provided a mechanizm for you to store your access tokens as masked CI / CD variables.

\[ [TOC](#table-of-contents) \]

#### Software Prerequisites
The following software will need to be installed on your local system in order to generate the necessary access tokens.

- [Python >= 3.7](https://www.python.org/downloads/)

- [git](https://git-scm.com/downloads)

- Base64; Windows users [go here](https://www.proxoft.com/base64.aspx)

- [CLASP](https://codelabs.developers.google.com/codelabs/clasp/#1)

\[ [TOC](#table-of-contents) \]

#### Generate a Token.Pickle
The token.pickle file is necessary for the `google_drive_handler.py` python module to push the latest question images and pull back a file with links to the newly created form. In order to create the `token.pickle` file, you need to install at least Python 3.7 and then run the provided `gen_gdrive_push_token.py` script. When the script is finished, you will need to run base64 on the `gdrive_images_token.pickle` file in order to produce a CI / CD compatible masked variable. The pipeline will take care of converting it back.

- In your web browser, navigate to your forked version of the `Knowledge Test Bank Template` and copy either the SSH or HTTPS clone link.

- Open a terminal, or command prompt, and clone your forked version by executing `git clone <URL>`.

- Navigate to the following path: `<Cloned Folder Name>/scripts/formal_exam_pres/google_suite/src/`.

- Note the location of your `credentials.json` file created in [Create an OAuth Client ID and Download credentials.json](#create-an-oauth-client-id-and-download-credentialsjson).

- Generate your token.pickle file by running `gen_gdrive_push_token.py`.

  - The first command-line parameter must be the path to your `credentials.json` file.
  
  - Optionally, specify `--out_dir OUT_DIR` to change the location of output file; the default is the current directory.

  - Optionally, specify `--scopes [SCOPE1, SCOPE2, ...]` to change the default scopes; the default should suffice unless you plan on doing something more than accessing Google Drive files.

  - Optionally, specify `--token_file_name TOKEN_FILE_NAME` to change the default output token's name.

  - If using default values, the command will look like the following; be sure to change `credentials.json` to its actual path:

  ```
  python3 gen_gdrive_push_token.py credentials.json
  ```

- Convert the contents of your `gdrive_images_token.pickle` file to a base64 string by running `base64 gdrive_images_token.pickle`

  - Windows users may have a slightly different syntax; refer to the [installation page](#software-prerequisites) for guidance.

- Copy the base64 output and then go to your forked version of the `Knowledge Test Bank Template` using your browser.

- Create another CI / CD masked variable.
  
  - Set the `Key` to `IMAGE_PUSH_TOKEN`.
  
  - Set the `Value` to the base64 output.
  
  - Check `Mask variable`.

\[ [TOC](#table-of-contents) \]

#### Generate Clasprc Tokens
Up until now, you have setup the ability to push and pull files from your Google Drive using the pipeline. These procedures will allow you to create the necessary tokens that enable the pipeline to push the latest Google Apps Script to your Google Drive and execute it. When script execution is finished, you will have exams for each work role in your test bank. Clasp is used to automate the execution of the Formal Exam Creator Google Apps script. In order to run scripts, you need to provide a global and local token.

- Create your global token by executing `clasp login`.
  
  - Go through the login process to your established Google Drive account.
  
  - After you finish accepting everything, the resulting `.clasprc.json` file will be located in your home directory.
  
  - Convert the contents of the file by running `base64 ~/.clasprc.json`.
  
  - Copy the output of the base64 command and create another CI / CD variable.
    
    - Set the `Key` to `CLASP_TOKEN_GLOBAL`.
    
    - Set the `Value` to the base64 output.
    
    - Check `Mask variable`.

- Create your local token by executing `clasp login --creds credentials.json`.
  
  - Ensure you replace `credentials.json` with the actual path to your `credentials.json` file.
  
  - Go through the login process to your established Google Drive account.
  
  - After you finish accepting everything, the resulting `.clasprc.json` file will either be located in the parent directory or the current.
  
  - Convert the contents of the file by running `base64 ../.clasprc.json`; you may have to adjust the path to `.clasprc.json`.
  
  - Copy the output of the base64 command and create another CI / CD variable.
    
    - Set the `Key` to `CLASP_TOKEN_LOCAL`.
    
    - Set the `Value` to the base64 output.
    
    - Check `Mask variable`.

\[ [TOC](#table-of-contents) \]

### Explanation of Exam Generation Scripts
There are two scripts involved with generating an exam: one that creates a list of exam questions and one that creates the exam. The first is needed in order for the second to know what to include on the exam. The explanations below speak in relation to the `google_suite_ci.yml` configuration.

\[ [TOC](#table-of-contents) \]

#### Script to build_exam_content
This script is responsible for creating the list of questions that will be included on an exam. The information below helps explain the configurable options of this script. Below is the output you will receive when using the `--help` option.

```text
usage: build_exam_content.py [-h] [--enable_mttl] [--font_file FONT_FILE] [--host HOST] [--log_file LOG_FILE] 
                             [--mttl_db_name MTTL_DB_NAME] [--nameof_mqf_file_suffix NAMEOF_MQF_FILE_SUFFIX] [--out_dir OUT_DIR]
                             [--port PORT] [--testbank_db_name TESTBANK_DB_NAME]
                             exam_gen_stats_file [exam_gen_stats_file ...] testbank_workrole_name

This will pick questions that will go on an exam.

positional arguments:
  exam_gen_stats_file   The path to the file storing the test bank's exam generation statistics.
  testbank_workrole_name
                        This specifies the work role test bank for exam generation; for example, planner, programmer, etc.
  font_file FONT_FILE   The path to the font file used during image creation.

optional arguments:
  -h, --help            show this help message and exit
  --enable_mttl         When specified, this enables usage of an MTTL MongoDB connector.
  --host HOST           This is the host name where the Mongo Database is running. The default is 'localhost'
  --log_file LOG_FILE   The name of the report file; the default is 'GenKnowledgeReport.json'.
  --mttl_db_name MTTL_DB_NAME
                        This is the name of the MTTL database and is used for DB connection purposes. The default is 'mttl'.
  --nameof_mqf_file_suffix NAMEOF_MQF_FILE_SUFFIX
                        The suffix name of the Master Question File (MQF); the test bank's name is prepended automatically. 
                        For example, a test bank named DEV will generate a file named 'DEV_MQF.json'. The default is 'MQF.json'.
  --out_dir OUT_DIR     The directory to store the resulting Generate Google Script Files report. The default is '.'.
  --port PORT           This is the port where the Mongo database is running. The default is '27017'.
  --testbank_db_name TESTBANK_DB_NAME
                        This is the name of the test bank database and is used for DB connection purposes. 
                        The default is knowledge_test_bank.
```

The three required parameters are `exam_gen_stats_file`, `testbank_workrole_name` and `font_file`.

- The `exam_gen_stats_file` parameter should be changed to the variable containing the path of the applicable test bank's `exam_gen_stats` file.

- The `testbank_workrole_name` parameter should be changed to the applicable test bank name.

- The `font_file` parameter is set to the `FONT_FILE` variable and should remain unchanged.

The exam_gen_stats_file is explained in the [Configure Exam Stats File](#configure-exam-stats-file) section. Unless you have other needs, such as a connection to an MTTL, the default values should be sufficient. One note about the `FONT_FILE` location, the path should be in relation to `scripts/formal_exam_pres/google_suite/src/` because this is the pipeline's current working directory.

\[ [TOC](#table-of-contents) \]

#### Script to generate_gsuite_exam
This script is responsible for interacting with your Google Drive in order to push question images, generate the exam and return links to the generated exam. The information below helps explain the configurable options of this script. Below is the output seen when using the `--help` option.

```text
usage: generate_gsuite_exam.py [-h] [--allow_edits] [--dont_collect_user_email] [--dont_make_form_a_quiz] 
                               [--dont_publish_summary_to_user] [--exam_description EXAM_DESCRIPTION] 
                               [--exam_is_organized_by_topic] [--exam_title EXAM_TITLE] [--exam_topics_are_sorted] 
                               [--image_ext IMAGE_EXT] [--limit_to_one_submission] [--min MIN] [--msec MSEC] [--out_dir OUT_DIR]
                               [--scopes SCOPES [SCOPES ...]] [--sec SEC] [--src_snip_dir SRC_SNIP_DIR] 
                               [--use_exam_at_index USE_EXAM_AT_INDEX] [--work_role WORK_ROLE]
                               testbank_json_file token_path dst_snip_dir_id work_role

This will generate a Google Form exam using the settings you specify when running this script and, if creating an exam by topic 
breakout, in your topic breakout configuration file.

positional arguments:
  testbank_json_file    This is a path to the file with the list of exams to be created. Each exam in the list contains its list 
                        of questions.
  token_path            This is the token needed for access to Google Drive to allow pushing images.
  dst_snip_dir_id       This is the Google Drive ID of the folder containing all the image files. The ID can be found in the 
                        folder's URL.
  work_role WORK_ROLE   The name of the work role being evaluated; should be the same name as the applicable test bank.

optional arguments:
  -h, --help            show this help message and exit
  --allow_edits         When true, users will be able to edit what was submitted. The default is 'False'.
  --dont_collect_user_email
                        When specified, the value is true and user e-mail address will NOT be collected. The default is 'False'.
  --dont_make_form_a_quiz
                        When specified, the value is true and the generated form is NOT set as a quiz. The default is 'False.
  --dont_publish_summary_to_user
                        When specified, the value is true and users are NOT given a link to view a summary of responses after 
                        submitting their own response. The default is 'False'.
  --exam_description EXAM_DESCRIPTION
                        A description of the generated exam. The default is ''.
  --exam_is_organized_by_topic
                        When specified, the value is true and results in question organization by topic; otherwise questions 
                        appear in no particular order. The default is False.
  --exam_title EXAM_TITLE
                        The title for the exam to be generated. The default is a combination of the test bank json file name, 
                        minus the extension, followed by 'Knowledge Exam'. So with a file name of
                        'planner_MQF_20201231.json', the title would be 'Planner MQF 20201231 Knowledge Exam'. Specifying 
                        something other than the default results in the title as entered.
  --exam_topics_are_sorted
                        When specified, the value is true and results in sorted topics; otherwise organized topics appear in no 
                        particular order. When true, it implies the exam is to be organized by topic. The
                        default is False.
  --image_ext IMAGE_EXT
                        This is the image file extension. The default is 'png'.
  --limit_to_one_submission
                        When specified, the value is true and users will only be able to make one submission. The default is 
                        'False'.
  --min MIN             This is a filter option to return the number of files created within X minutes.The default is '1'
  --msec MSEC           This is a filter option to return the number of files created within X microseconds.The default is '0'
  --out_dir OUT_DIR     This is the name of the folder where output files will be saved.
  --scopes SCOPES [SCOPES ...]
                        The needed scopes for accessing Google API services. 
                        The default is '['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/drive.file']'.
  --sec SEC             This is a filter option to return the number of files created within X seconds.The default is '30'
  --src_snip_dir SRC_SNIP_DIR
                        This is the folder containing all images, needed for the questions in the MQL, to be uploaded. 
                        The default is 'code_snippets'.
  --use_exam_at_index USE_EXAM_AT_INDEX
                        This specifies which exam to use for test generation. The number represents the index of the exams list 
                        within the specified testbank_json_file. The default is '0'.
```

The four required parameters are `testbank_json_file`, `token_path`, `dst_snip_dir_id` and `work_role`.

- The `testbank_json_file` parameter uses the shell variable called `EXAM_LIST_FILE` which is set after the `build_exam_content` script finishes and should remain unchanged.

- The `token_path` parameter is set to the `IMAGE_TOKEN_FILE` variable and, since the token file is generated from your CI / CD `IMAGE_PUSH_TOKEN` variable, this should remain unchanged.

- The `dst_snip_dir_id` parameter is set to the CI / CD `DST_IMAGE_DIR_ID` variable you created in [Local Exam Setup](#local-exam-setup) and shoud remain unchanged.

- The `work_role` parameter should be changed to the name of the applicable test bank.

For the most part, you can use the default values for the optional parameters; however, the following options allow customizing the format and content of the generated exam. Specify one or more of the following to customize the exam's format and content.

- `--exam_description` allows you to specify text for examinees regarding the exam they are about to take or anything else you may want them to know.

- `--exam_title EXAM_TITLE` allows you to customize the title of your exam; the default behavior is to combine the name of the generated exam list, minus the .json extention, with `Knowledge Exam`. For example, if you had an exam list file named `Planner Exams 20210101.json`, the title would read `Planner 20210101 Knowledge Exam`. Any specified value for `--exam_title` will be displayed as entered.

- `--exam_is_organized_by_topic` is a flag that, when specified, organizes the questions according to the value in their `topic` key; the default behavior creates the exam in the order the questions are received.

- `--exam_topics_are_sorted` is a flag that, when specified, sorts the organized topics in ascending order; the `--exam_is_organized_by_topic` flag is implied so you do not have to specify both.

- `--use_exam_at_index` allows specifying which exam in the exam list file to use. The default behavior is to use the first exam in the list. For situations where you decide to generate multiple exams, you can specify which exam is to be generated by indicating the exam's index number.

\[ [TOC](#table-of-contents) \]

### Configure Exam Stats File
The test generation configuration file defines certain settings, used by the `build_exam_content` Python script, that determine how your exam question list is built. By including one exam stats file per test bank, you can create seperate configurations for each. The information in this section will help you understand and build the necessary stats files.

We will use the following test banks example to help explain this process. The `test-banks` folder is in the root of your knowledge test bank GitLab repository. Each folder within `test-banks` is considered an individual test-bank; therefore, this example has two test-banks: CCD and PO.

```text
test-banks
|
|- CCD
|  |- loops.question.json
|  |- pointers.question.json
|  |- CONDITIONAL-GROUP
|  |  |- if_statements.question.json
|  |  |- nested_if.question.json
|  |  |- switch_statements.question.json
|  |- error_handling.question.json
|
|- PO
|  |- backlog_refinement.question.json
|  |- sprint_planning.question.json
|  |- sprint_ceremonies.question.json
```

Within your knowledge test bank repository, there are two `exam_gen_stats` files located in the `scripts/formal_exam_pres/exam_content_builder/` directory. For each test bank, rename the existing `exam_gen_stats_*.json` file so it is indicative of one of your test bank names. If you only have one test bank, then you will need to remove one of the `exam_gen_stats` files. In our example, we will make the following changes within the `scripts/formal_exam_pres/exam_content_builder/` directory.

- The `exam_gen_stats_planner.json` file will be renamed to `exam_gen_stats_ccd.json`

- The `exam_gen_stats_programmer.json` file will be renamed to `exam_gen_stats_po.json`

For each additional test bank, add another file in the same directory as the others.

Within each `exam_gen_stats` file, verify the settings are set to your needs. The following is an example of the `exam_gen_stats` file content.

```json
1  | {
2  |     "exams_generated": 0,
3  |     "exams_to_gen": 1,
4  |     "time_allowed": 90,
5  |     "delta": 0.2,
6  |     "topics": {
7  |         "CriticalThinking": 1,
8  |         "Flowcharting": 1
9  |     }
10 | }
```

- Line 2 indicates the number of exams that have been generated and should be left alone.

- Line 3 indicates the number of exams that should be created and can be changed to your needs.

- Line 4 indicates the amount of time examinees have to complete their exam and can be changed to your needs.

- Line 5 is used by the exam builder to help in its decision-making process and should be left alone.

- Line 6 is used when you desire to have your exam organized by topic and determines the number of questions to include per topic.

  - If you do not desire to use topic organization, then lines 6 - 9 can be removed; otherwise, change the topic names according to those used in your `question.json` file topic key.

  - The structure of your question.json files is discussed in the `test-banks` guide.

\[ [TOC](#table-of-contents) \]

### CI / CD Pipeline Configuration
In order to get your test bank content generated in an exam, you need to modifiy the `scripts/formal_exam_pres/google_suite/google_suite_ci.yml` file with an entry for each of your test banks; refer to the `test-banks` guide for instructions on initial setup of your test bank. In order to explain this process, we will use the test banks example in [Configure Exam Stats File](#configure-exam-stats-file).

The relavent lines from the `scripts/formal_exam_pres/google_suite/google_suite_ci.yml` file looks like below.

```yml
1  | . . .
2  |
3  | # The script 'build_exam_content.py' creates a test bank json file, containing the exam's questions, and sends its 
4  | # path to standard out. This path is fed into the script 'generate_gsuite_exam.py' so the exam questions can be sent 
5  | # to the Google script 'ExamCreator.js' as an input parameter during clasp execution.
6  | # 
7  | # For each work role in your test bank, create an anchor that can be used in the scripts section of the 'google_suite' 
8  | # job; an example can be seen in '.deploy_planner_exam' and '.deploy_programmer_exam'.
9  | .deploy_planner_exam: &deploy_planner_exam
10 |   # Build the exam list and save the output file name
11 |   - EXAM_LIST_FILE=$(python3 $BUILD_EXAM $PLANNER_STATS_FILE planner $FONT_FILE)
12 |   # Generate the exam using the exam list file just created
13 |   - python3 generate_gsuite_exam.py $EXAM_LIST_FILE $IMAGE_TOKEN_FILE $DST_IMAGE_DIR_ID planner --exam_description "You have X min to complete this exam. Good luck."
14 | 
15 | .deploy_programmer_exam: &deploy_programmer_exam
16 |   # Build the exam list and save the output file name
17 |   - EXAM_LIST_FILE=$(python3 $BUILD_EXAM $PROGRAMMER_STATS_FILE programmer $FONT_FILE)
18 |   # Generate the exam using the exam list file just created
19 |   - python3 generate_gsuite_exam.py $EXAM_LIST_FILE $IMAGE_TOKEN_FILE $DST_IMAGE_DIR_ID programmer --exam_topics_are_sorted --exam_description "You have X min to complete this exam. Good luck."
20 | 
21 | . . .
22 | google_suite:
23 |   variables:
24 |     GSUITE_DIR: "<path to google_suite_dir>"
25 |     CLASP_DIR: "<directory where clasp can be run>"
26 |     OUT_DIR: "<directory for output files>"
27 |     IMAGE_TOKEN_FILE: "<name of the token.pickle file>"
28 |     # These paths are in relation to the directory 'scripts/formal_exam_pres/google_suite/src/' because this is where
29 |     # the current working directory needs to be in order to execute the ExamCreator.js script using clasp
30 |     BUILD_EXAM: "../../exam_content_builder/build_exam_content.py"
31 |     FONT_FILE: "../../exam_content_builder/support/font_files/monaco.ttf"
32 |     # Be sure to add variables for any additional work roles in your test bank
33 |     PLANNER_STATS_FILE: "../../exam_content_builder/exam_gen_stats_planner.json"
34 |     PROGRAMMER_STATS_FILE: "../../exam_content_builder/exam_gen_stats_programmer.json"
35 | 
36 | . . .
37 |
38 |   script:
39 |     . . .
40 | 
41 |     # Be sure to add any additional form creation anchors below as needed
42 |     - *deploy_planner_exam
43 |     - *deploy_programmer_exam
44 |     # This makes uploaded artifacts easier to access and should be the last line under script:
45 |     - cp -r $OUT_DIR $CI_PROJECT_DIR
46 | 
47 | . . .
48 | 
```

Refering to the `google_suite_ci.yml` example above, lines 9 & 15 are what is known as anchors. Anchors allow you to define something that can be referenced later. For each test bank, you need to create an anchor similar to the examples on lines 9 & 15. Therefore, our example `google_suite_ci.yml` will need the following modifications.

- Line 9 changed to ` .deploy_ccd_exam: &deploy_ccd_exam`.

- Line 11

  - Replace `$PLANNER_STATS_FILE` with `$CCD_STATS_FILE`.

  - Replace `planner` with `ccd`.

- Line 13

  - Replace `planner` with `ccd`.

  - Change the description to something you want; keep in mind, this will show up on the first page of the exam. The rest can stay the same unless you want to add different form formatting options. If desired, you can remove the description option.

- Line 15 changed to ` .deploy_po_exam: &deploy_po_exam`.

- Line 17 

  - Replace `$PROGRAMMER_STATS_FILE` with `$PO_STATS_FILE`.

  - Replace `programmer` with `po`.

- Line 19

  - Replace `programmer` with `po`.
  
  - Change the description to something you want; keep in mind, this will show up on the first page of the exam. The rest can stay the same unless you don't want your questions organized by topic or prefer other form formatting options. If desired, you can remove the description option.

For each additional test bank, you will need to add an anchor with these settings.

Lines 33 & 34 define variables for the path to each test generation configuration file. The test generation configuration file defines how many exams are to be generated, how many questions per topic should appear, etc. You will need one configuration file per test bank; this allows you to have separate configurations for each test bank. So, in our example we need to do the following.

- Line 33 changed to `     CCD_STATS_FILE: "../../exam_content_builder/exam_gen_stats_ccd.json"`.

- Line 34 changed to `     PO_STATS_FILE: "../../exam_content_builder/exam_gen_stats_po.json"`.

For each additional test bank, you will need to add a variable storing the relative location to your exam_gen_stats file; this will be relative to the `scripts/formal_exam_pres/google_suite/src/` directory.

Lines 42 & 43 is where you will refer to the anchors that were created on lines 9 & 15. So, in our example we need to make the following changes.

- Line 42 changed to `     - *deploy_ccd_exam`.

- Line 43 changed to `     - *deploy_po_exam`.

For each additional test bank, you will need to add references to the anchors you create. When adding additional references to anchors, ensure line 45 remains the last line in the scripts section. If you only have one test bank, then you should remove lines 15 - 19, 34, and 43.

\[ [TOC](#table-of-contents) \]

# Formal Exam Presentation Deployment
This explains the process of deploying an exam.

\[ [TOC](#table-of-contents) \]

## GitLab Continuous Integration/Continuous Delivery (CI/CD) Pipeline
The GitLab CI/CD pipeline uses the Formal Exam component to collect question content and generate exams for each work role. With the exception of a few manual form setting changes, all you have to do is click the manual task to run the job and your exams will appear in your Google Drive. The pipeline will also save an artifact with links to access each exam.

\[ [TOC](#table-of-contents) \]

### View Pipeline Artifacts
To view the artifacts:

- From with the `CI / CD` pipeline view, select the `google_suite` job from the last circle button on the right.

- A `Browse` link to collected artifacts will appear to the right of the terminal output, click this button.
  <br><br>
  ![Fig 19](images/fig_19_exam-pres_PipelineArtifacts.png)
  <br><br>

\[ [TOC](#table-of-contents) \]

## Exam Creation
This process has two parts, generate the exams and configure exam settings.

\[ [TOC](#table-of-contents) \]

### Generate Exams
Perform the following to generate your exams.

- Login to GitLab and browse to your knowledge test bank repository; this will be the one forked from our `Knowledge Test Bank Template`.

- In the left vertical menu, click `CI / CD`.

- If the master branch hasn't run in a while then click the `Run Pipeline` button and run the `master` pipeline.

- When the deploy stage is reached, hover your mouse over the deploy stage circle, the one on the far right, and click the play button for the `google_suite` job.
  <br><br>
  ![Fig 20](images/fig_20_exam-pres_PipelineRun.png)
  <br><br>
- When the `google_suite` job finishes, an html artifact will be available with links to the forms just created.

\[ [TOC](#table-of-contents) \]

### Configure Exam Settings
Unfortunately, there are some limitations to Google Forms; one of them is the ability to set some settings. This information will enable your exams to immediately release grades, show examinee's their score, and provide feeback on right and wrong answers.

- From the pipeline `google_suite` job artifact, open the html file for the desired form and click the `Edit Form` link (ensure you are logged into your Google Drive or it will not open in edit mode).

- Modify the following settings by clicking on the icon in the upper right corner that looks like a gear.

  - In the `Quizzes` tab, by default `Make this a quiz` is enabled, `Release grade->Later, after manual review` is selected and no options are selected under `Respondent can see:`.

    - Change `Later, after manual review` to `Immediately after each submission`.

    - Select all options under `Respondent can see:`; your settings window should look like the figure below.

    - When you're finished, click the `Save` button.
  <br><br>
  ![Fig 21](images/fig_21_exam-pres_FormSettings.png)
  <br><br>

\[ [TOC](#table-of-contents) \]

## Exam Distribution
This explains how to get a link to the form that examinees can access to take their exam.

- From the pipeline `google_suite` job artifact, open the html file for the desired form and copy the `Take the Exam` link.

- Distribute this link to anyone needing to take the exam.

\[ [TOC](#table-of-contents) \]
