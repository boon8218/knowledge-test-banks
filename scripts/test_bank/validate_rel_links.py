#!/usr/bin/python3
import os
import re
import json
import pymongo
from bson.objectid import ObjectId

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db_mttl = client.mttl
db = client['knowledge_test_bank']

error = False
error_log = {}

def logerror(question: dict, missing_rel_link: bool=False, invalid_rel_link: bool=False):
    global error_log, error
    if missing_rel_link:
        error_log[question['question_name']] = 'Missing \'rel-link_id\' field'
    elif invalid_rel_link:
        error_log[question['question_name']] = 'Invalid Rel-Link value'
    else:
        error_log[question['question_name']] = 'Rel-Link does not exist in MTTL'
    error = True
    return

def main():
    global error_log, error
    collection_list = db.list_collection_names()
    for collection in collection_list:
        for question in db[collection].find({}):
            if 'rel-link_id' in question and re.match("^[0-9a-f]{24}$", question['rel-link_id'], re.IGNORECASE):
                rel_link = db_mttl.rel_links.find_one({'_id': ObjectId(question['rel-link_id'])})
                if not rel_link: 
                    logerror(question)    
            elif 'rel-link_id' in question and not re.match("^[0-9a-f]{24}$", question['rel-link_id'], re.IGNORECASE):
                # In case the rel-link's value is something invalid such as an empty string, whitespace, etc.
                logerror(question, invalid_rel_link=True)
            elif 'rel-link_id' not in question:
                logerror(question, missing_rel_link=True)

    if len(error_log) > 0:
        with open('validate_rel_links.json', 'w') as logfile:
            json.dump(error_log, logfile, indent=4)
    if error:
        exit(1)


if __name__ == "__main__":
    main()